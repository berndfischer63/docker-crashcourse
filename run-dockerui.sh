#!/bin/bash

echo "############################################################"
echo "### start-dockerui-container.sh => begin"
echo "###"

docker run --name=dockerui -d -p 9000:9000 -v /var/run/docker.sock:/docker.sock dockerui/dockerui:0.7.0 -e /docker.sock

echo "###"
echo "### start-dockerui-container.sh => finished"
echo "############################################################"

#!/bin/bash

SCRIPT_NAME=$0

echo "############################################################"
echo "### $SCRIPT_NAME => begin"
echo "###"

ansible-playbook -i inventory -u duser -k --become --ask-become-pass playbook.yaml

echo "###"
echo "### $SCRIPT_NAME => finished"
echo "############################################################"
